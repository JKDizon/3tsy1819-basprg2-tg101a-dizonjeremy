#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));

	int heroHP;
	int heroMaxDMG;
	int heroMinDMG;
	int heroBaseDMG;
	int heroDealtDMG;
	int enemyHP;
	int enemyMaxDMG;
	int enemyMinDMG;
	int enemyBaseDMG;
	int enemyDealtDMG;
	char heroMove;
	int enemyMove;

	cout << "Initializing Zephyr (Hero)..." << endl << endl;
	cout << "Input HP for Zephyr: ";
	cin >> heroHP;
	cout << "Input minimum damage for Zephyr: ";
	cin >> heroMinDMG;
	do {
		cout << "Input maximum damage for Zephyr. Value must be greater than or equal to minimum damage: ";
		cin >> heroMaxDMG;
	} while (heroMaxDMG < heroMinDMG);

	heroBaseDMG = rand() % (heroMaxDMG - heroMinDMG + 1) + heroMinDMG;

	system("cls");

	cout << "Initializing Soul Reaper (Enemy)..." << endl << endl;
	cout << "Input HP for Soul Reaper: ";
	cin >> enemyHP;
	cout << "Input minimum damage for Soul Reaper: ";
	cin >> enemyMinDMG;
	do {
		cout << "Input maximum damage for Soul Reaper. Value must be greater than or equal to minimum damage: ";
		cin >> enemyMaxDMG;
	} while (enemyMaxDMG < enemyMinDMG);

	enemyBaseDMG = rand() % (enemyMaxDMG - enemyMinDMG + 1) + enemyMinDMG;

	while (heroHP > 0 && enemyHP > 0)
	{
		system("cls");
		do {
			cout << "Zephyr HP: " << heroHP << endl;
			cout << "Soul Reaper HP: " << enemyHP << endl << endl;
			cout << "Input your move: " << endl;
			cout << "===============" << endl;
			cout << "     [a] = Attack" << endl;
			cout << "     [d] = Defend" << endl;
			cout << "     [w] = Wild Attack" << endl;
			cin >> heroMove;
			system("cls");
		} while (heroMove != 'a' && heroMove != 'd' && heroMove != 'w' && heroMove != 'A' && heroMove != 'D' && heroMove != 'W');

		enemyMove = rand() % 3 + 1;

		switch (heroMove)
		{
		case 'a': case 'A':
			cout << "Zephyr attacks..." << endl;
			break;
		case 'd': case 'D':
			cout << "Zephyr defends..." << endl;
			break;
		case 'w': case 'W':
			cout << "Zephyr prepares for a special attack..." << endl;
			break;
		}

		switch (enemyMove)
		{
		case 1:
			cout << "Soul Reaper attacks..." << endl << endl;
			break;
		case 2:
			cout << "Soul Reaper defends..." << endl << endl;
			break;
		case 3:
			cout << "Soul Reaper prepares for a special attack..." << endl << endl;
			break;
		}

		if (enemyMove == 2 && heroMove == 'a' || heroMove == 'A')
		{
			heroDealtDMG = heroBaseDMG / 2;
			cout << "Zephyr dealt " << heroDealtDMG << " damage." << endl;
			cout << "Soul Reaper: Death follows." << endl << endl;

			enemyHP = enemyHP - heroDealtDMG;

			system("pause");
		}
		else if (enemyMove == 3 && heroMove == 'a' || heroMove == 'A')
		{
			enemyDealtDMG = enemyBaseDMG * 2;
			cout << "Zephyr dealt " << heroBaseDMG << " damage." << endl;
			cout << "Soul Reaper dealt " << enemyDealtDMG << " damage. CRITICAL!" << endl;
			cout << "Soul Repear: Let the bloodbath begin!" << endl << endl;

			heroHP = heroHP - enemyDealtDMG;
			enemyHP = enemyHP - heroBaseDMG;

			system("pause");
		}
		else if (enemyMove == 1 && heroMove == 'a' || heroMove == 'A')
		{
			cout << "Zephyr dealt " << heroBaseDMG << " damage." << endl;
			cout << "Soul Reaper dealt " << enemyBaseDMG << " damage." << endl << endl;

			heroHP = heroHP - enemyBaseDMG;
			enemyHP = enemyHP - heroBaseDMG;

			system("pause");
		}

		if (enemyMove == 1 && heroMove == 'd' || heroMove == 'D')
		{
			enemyDealtDMG = enemyBaseDMG / 2;
			cout << "Soul Reaper dealt " << enemyDealtDMG << " damage." << endl;
			cout << "Zephyr: Bird brain." << endl << endl;

			heroHP = heroHP - enemyDealtDMG;

			system("pause");
		}
		else if (enemyMove == 3 && heroMove == 'd' || heroMove == 'D')
		{
			heroDealtDMG = heroBaseDMG * 2;
			cout << "Zephyr countered and dealt " << heroDealtDMG << " damage. CRITICAL!" << endl;
			cout << "Zephyr: Bird brain." << endl << endl;

			enemyHP = enemyHP - heroDealtDMG;

			system("pause");
		}
		else if (enemyMove == 2 && heroMove == 'd' || heroMove == 'D')
		{
			cout << "They anticipate each other's moves..." << endl << endl;
			system("pause");
		}

		if (enemyMove == 1 && heroMove == 'w' || heroMove == 'W')
		{
			heroDealtDMG = heroBaseDMG * 2;
			cout << "Zephyr dealt " << heroDealtDMG << " damage. CRITICAL!" << endl;
			cout << "Soul Reaper dealt " << enemyBaseDMG << " damage." << endl;
			cout << "Zephyr: You're riding a dangerous wind!" << endl << endl;

			heroHP = heroHP - enemyBaseDMG;
			enemyHP = enemyHP - heroDealtDMG;

			system("pause");
		}
		else if (enemyMove == 3 && heroMove == 'w' || heroMove == 'W')
		{
			heroDealtDMG = heroBaseDMG * 2;
			enemyDealtDMG = enemyBaseDMG * 2;
			cout << "Zephyr dealt " << heroDealtDMG << " damage. CRITICAL!" << endl;
			cout << "Soul Reaper dealt " << enemyDealtDMG << " damage. CRITICAL!" << endl;
			cout << "Zephyr: You're riding a dangerous wind!" << endl;
			cout << "Soul Repear: Let the bloodbath begin!" << endl << endl;

			heroHP = heroHP - enemyDealtDMG;
			enemyHP = enemyHP - heroDealtDMG;

			system("pause");
		}
		else if (enemyMove == 2 && heroMove == 'w' || heroMove == 'W')
		{
			enemyDealtDMG = enemyBaseDMG * 2;
			cout << "Soul Reaper countered and dealt " << enemyDealtDMG << " damage. CRITICAL!" << endl;
			cout << "Soul Reaper: Death follows." << endl << endl;

			heroHP = heroHP - enemyDealtDMG;

			system("pause");
		}
	}

	system("cls");

	if (heroHP <= 0 && enemyHP > 0)
	{
		cout << "Soul Reaper: Souls will be mine!" << endl;
		cout << "Soul Reaper is the WINNER!" << endl << endl;
		system("pause");
	}
	else if (enemyHP <= 0 && heroHP > 0)
	{
		cout << "Zephyr: I bring light!" << endl;
		cout << "Zephyr is the WINNER!" << endl << endl;
		system("pause");
	}
	else
	{
		cout << "Zephyr: It looks like we over did our selfs." << endl;
		system("pause");

		cout << "Soul Reaper: You're right." << endl;
		system("pause");

		cout << "Soul Reaper: Looks like this time it's my soul that will be gone." << endl;
		system("pause");

		cout << "Zephyr: It's okay your not the only one who's gonna lose his soul." << endl;
		system("pause");

		cout << "Zephyr: That was a good fight my friend but it's time to say our goodbyes now." << endl;
		system("pause");

		cout << "Soul Reaper: See you on the other side." << endl;
		system("pause");
	}
	
	return 0;
}