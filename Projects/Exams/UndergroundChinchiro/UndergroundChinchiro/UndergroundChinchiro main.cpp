#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;

struct Roll
{
	string type; //Snake Eyes (1-1-1), Triples (6-6-6), 4-5-6, Pair (6-x-x), 1-2-3, Bust, Pisser
	int rank; //1, 2, 3, 4, 5, 6, 7
	int value; //1, 2, 3, 4, 5, 6
	int dice[3]; //1, 2, 3, 4, 5, 6
};

int Wager(int perica);
Roll Dice(Roll character, int tries);
Roll DiceTries(Roll character);
int Chinchiro(Roll dealer, Roll player, int bet);
void Ending(int perica);

int main()
{
	int perica = 90000;
	int roundCount = 1;
	int bet;
	Roll Ohtsuki;
	Roll Kaiji;

	srand(time(NULL));

	do
	{
		system("cls");
		cout << "ROUND " << roundCount << " of 10" << endl
			<< "Perica: " << perica << endl;
		system("pause");

		bet = Wager(perica);

		system("cls");
		cout << "Ohtsuki (Dealer) rolls..." << endl;
		system("pause");

		Ohtsuki = DiceTries(Ohtsuki);

		system("cls");

		if (Ohtsuki.rank == 7)
		{
			cout << "Ohtsuki (Dealer) automatically loses." << endl
				<< "Kaiji (Player) wins " << bet << endl;
			system("pause");
			perica = perica + bet;
		}
		else
		{
			cout << "Kaiji (Player) rolls..." << endl;
			system("pause");

			Kaiji = DiceTries(Kaiji);

			system("cls");
			perica = perica + Chinchiro(Ohtsuki, Kaiji, bet);
		}

		roundCount++;
	} while (perica > 100 && roundCount <= 10);

	Ending(perica);

	system("pause");
	return 0;
}

int Wager(int perica)
{
	int bet = 0;

	do
	{
		system("cls");

		cout << "Perica: " << perica << endl
				<< "Minimum Bet: 100" << endl
				<< "Input bet: ";
		cin >> bet;
	} while (bet < 100 || bet > perica);

	return bet;
}

Roll Dice(Roll character, int tries)
{
	int pisser;
	int face[6] = { 0 };

	pisser = rand() % 100 + 1;

	if (pisser > 5)
	{
		for (int i = 0; i < 3; i++)
		{
			character.dice[i] = rand() % 6 + 1;
			face[character.dice[i]-1]++;
		}

		for (int i = 0; i < 6; i++)
		{
			if (face[i] == 3)
			{
				switch (character.dice[1])
				{
				case 1:
					character.type = "Snake Eyes";
					character.rank = 1;
					break;
				case 2: case 3: case 4: case 5: case 6:
					character.type = "Triple";
					character.rank = 2;
					character.value = character.dice[1];
					break;
				}
				break;
			}
			else if (face[i] == 2)
			{
				character.type = "Pair";
				character.rank = 4;
				character.value = i + 1;
				break;
			}
			else if (face[i] <= 1)
			{
				character.type = "Bust";
				character.rank = 6;
			}
		}

		if (face[0] == 1 && face[1] == 1 && face[2] == 1)
		{
			character.type = "1-2-3";
			character.rank = 5;
		}

		if (face[3] == 1 && face[4] == 1 && face[5] == 1)
		{
			character.type = "4-5-6";
			character.rank = 3;
		}

		cout << endl << character.type << ": ";
		for (int i = 0; i < 3; i++)
		{
			cout << character.dice[i] << " ";
		}
		if (character.rank == 2 || character.rank == 4)
		{
			cout << "(" << character.value << ")";
		}
		if (character.rank == 6)
		{
			cout << endl << "Tries left: " << tries;
		}
	}

	else {
		character.type = "Pisser";
		character.rank = 7;

		cout << endl << character.type;
	}

	cout << endl;
	system("pause");
	return character;
}

Roll DiceTries(Roll character)
{
	int tries = 3;

	do
	{
		character = Dice(character, tries);
		tries--;
	} while (character.rank == 6 && tries >= 1);

	return character;
}

int Chinchiro(Roll dealer, Roll player, int bet)
{
	cout << "OHTSUKI (Dealer):" << endl
		<< "==================================" << endl
		<< dealer.type << ": ";
	for (int i = 0; i < 3; i++)
	{
		cout << dealer.dice[i] << " ";
	}
	if (dealer.rank == 2 || dealer.rank == 4)
	{
		cout << "(" << dealer.value << ")";
	}

	cout << endl << endl << "KAIJI:" << endl
		<< "==================================" << endl;
	if (player.rank < 7)
	{
		cout << player.type << ": ";
		for (int i = 0; i < 3; i++)
		{
			cout << player.dice[i] << " ";
		}
		if (player.rank == 2 || player.rank == 4)
		{
			cout << "(" << player.value << ")";
		}
	}
	else
	{
		cout << player.type;
	}

	cout << endl << endl;
	system("pause");

	cout << endl << endl;

	switch (player.rank)
	{
	case 1:
		bet = bet * 5;
		break;
	case 2:
		bet = bet * 3;
		break;
	case 3: case 5:
		bet = bet * 2;
		break;
	}

	if (player.rank < dealer.rank)
	{
		cout << "Kaiji wins " << bet << " Perica";
	}
	else if (player.rank == 5 && dealer.rank == 5)
	{
		cout << "Kaiji wins " << bet << " Perica";
	}
	else if (player.rank == 6 && dealer.rank == 6)
	{
		cout << "Kaiji wins " << bet << " Perica";
	}
	else if (player.rank == 7 && dealer.rank == 5)
	{
		cout << "Kaiji wins " << bet << " Perica";
	}
	else if (player.rank == dealer.rank)
	{
		if (player.value > dealer.value)
		{
			cout << "Kaiji wins " << bet << " Perica";
		}
		else if (player.value < dealer.value)
		{
			cout << "Kaiji loses " << bet << " Perica";
			bet = bet * -1;
		}
		else
		{
			cout << "DRAW";
			bet = 0;
		}
	}
	else if (player.rank == 7 && dealer.rank == 6)
	{
		cout << "DRAW";
		bet = 0;
	}
	else
	{
		cout << "Kaiji loses " << bet << " Perica";
		bet = bet * -1;
	}

	cout << endl;
	system("pause");

	return bet;
}

void Ending(int perica)
{
	system("cls");

	cout << "Perica: " << perica << endl << endl;

	if (perica >= 500000)
	{
		cout << "Congratulations, you won!" << endl << endl;
	}
	else if (perica < 500000 && perica > 90000)
	{
		cout << "Great! You earned some extra Perica, but you could have earned more." << endl << endl;
	}
	else if (perica <= 90000)
	{
		cout << "You win some, you lose some." << endl << endl;
	}
	else if (perica <= 100 && perica >= 0)
	{
		cout << "You have lost all of your Perica." << endl << endl;
	}
	else if (perica < 0)
	{
		cout << "You have to borrow Perica from Ohtsuki. You'll be in BIG trouble if you don't pay him back." << endl << endl;
	}
}