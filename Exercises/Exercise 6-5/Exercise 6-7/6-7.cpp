#include <iostream>

using namespace std;

int main()
{
	int numbers[10] = { -20,
						0,
						7,
						3,
						2,
						1,
						8,
						-3,
						6,
						3 };
	int largest = 0;

	for (int i = 0; i < 10; i++)
	{
		if (numbers[i] > largest)
			largest = numbers[i];
	}
	
	cout << "The largest number is: " << largest << endl;

	system("pause");
	return 0;
}