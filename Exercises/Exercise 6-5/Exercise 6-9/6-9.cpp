#include<iostream>

using namespace std;

int main()
{
	int numbers[10];
	int temp;

	cout << "Enter 10 unsorted integers: " << endl;

	for (int i = 0; i < 10; i++)
	{
		cout << "[" << i << "] = ";
		cin >> numbers[i];
	}
	cout << "Unsorted list = ";

	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << ", ";
	}
	cout << endl;
	cout << "Sorting... " << endl;
	
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 10 - i - 1; ++j)
		{
			if (numbers[j] > numbers[j + 1])
			{
				numbers[j] = numbers[j] + numbers[j + 1];
				numbers[j + 1] = numbers[j] - numbers[j + 1];
				numbers[j] = numbers[j] - numbers[j + 1];
			}
		}
	}

	cout << "Sorted List = ";

	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << ", ";
	}

	cout << endl;

	system("pause");
	return 0;
}