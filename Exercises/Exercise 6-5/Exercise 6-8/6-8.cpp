#include<iostream>

using namespace std;

int main()
{
	int numbers[10];
	int temp;
	
	cout << "Enter 10 unsorted integers: " << endl;

	for (int i = 0; i < 10; i++)
	{
		cout << "[" << i << "] = ";
		cin >> numbers[i];
	}
	cout << "Unsorted list = ";

	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << ", ";
	}
	cout << endl;
	cout << "Sorting... " << endl;

	for (int i = 0; i<10; i++)
	{
		for (int j = i + 1; j<10; j++)
		{
			if (numbers[i]>numbers[j])
			{
				temp = numbers[i];
				numbers[i] = numbers[j];
				numbers[j] = temp;
			}
		}
	}

	cout << "Sorted List = ";

	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << ", ";
	}
	
	cout << endl;

	system("pause");
	return 0;
}