#include <iostream>
#include <string>
#include <stdlib.h> 
#include <time.h> 

using namespace std;

int main() {
	string items[] = { "Red Potion", 
						"Blue Potion", 
						"Yggdrasil Leaf", 
						"Elixir", 
						"Teleport Scroll", 
						"Red Potion", 
						"Red Potion", 
						"Elixir"
						};
	int random;
	char exit = 'a';

	srand(time(NULL));

	do 
	{
		random = rand() % 8;

		cout << items[random] << endl;
		cout << "Press any key to continue or X to exit" << endl;
		cin >> exit;
	} while (exit != 'x');

	return 0;
}