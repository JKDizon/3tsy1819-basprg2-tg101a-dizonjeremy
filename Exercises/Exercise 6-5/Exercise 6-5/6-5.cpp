#include <iostream>
#include <string>

using namespace std;

int main() {
	string items[] = { "Red Potion", 
						"Blue Potion", 
						"Yggdrasil Leaf", 
						"Elixir", 
						"Teleport Scroll",
						"Red Potion", 
						"Red Potion", 
						"Elixir" 
						};
	string itemsSearch;
	int i;
	int counter = 0; 

	cout << "Search for: ";
	getline(cin, itemsSearch);

	for (i = 0; i <= 7; i++) 
	{
		if (itemsSearch == items[i]) 
		{
			counter++;
		}
	}

	if (counter > 0) 
	{
		cout << "You Have " << counter << " " << itemsSearch << endl;
	}
	else 
	{
		cout << itemsSearch << " Does not exist." << endl;
	}

	system("pause");
	return 0;
}