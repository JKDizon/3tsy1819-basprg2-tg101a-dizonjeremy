#include <iostream>
#include <string>

using namespace std;

int main() 
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	string itemsSearch;
	bool searchResults = false;

	cout << "Search for: ";
	getline(cin, itemsSearch);

	for (int i = 0; i <= 7; i++) 
	{
		if (itemsSearch == items[i]) 
		{
			searchResults = true;
		}
	}

	if (searchResults) 
	{
		cout << itemsSearch << " exists." << endl;
	}
	else 
	{
		cout << itemsSearch << " does not exist." << endl;
	}

	system("pause");
	return 0;
}