#include <iostream>
#include <string>

using namespace std;

int main() 
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	string itemsSearch;
	int i;

	cout << "Search for: ";
	getline(cin, itemsSearch);

	for (i = 0; i <= 7; i++)
	{
		if (itemsSearch == items[i]) 
		{
			break;
		}
	}

	if (i <= 7) 
	{
		cout << "Item index: " << i << endl;
	}
	else 
	{
		cout << itemsSearch << " does not exist." << endl;
	}

	system("pause");
	return 0;
}