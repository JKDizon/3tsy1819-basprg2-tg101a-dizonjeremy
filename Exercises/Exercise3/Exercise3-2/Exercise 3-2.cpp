#include <iostream>

int main()

{
	int x, y, z;

	std::cout << "Input x: "; std::cin >> x;
	std::cout << "Input y: "; std::cin >> y;

	z = x + y;
	std::cout<< std::endl << x << " + " << y << " = " << z;

	z = x - y;
	std::cout<< std::endl << x << " - " << y << " = " << z;

	z = x * y;
	std::cout<< std::endl << x << " * " << y << " = " << z;

	z = x / y;
	std::cout<< std::endl << x << "/" << y << "=" << z;

	z = x % y;
	std::cout <<std::endl  << x << "%" << y << "=" << z;

	system("pause");
	
	return 0;

}