#include <iostream>
#include <stdlib.h> 
#include <time.h>

using namespace std;

int fillArray(int numbers[20]);

int main()
{
	int numbers[20];

	srand(time(NULL));

	fillArray(numbers);

	cout << "Randomly Generated Array = ";

	for (int i = 0; i < 20; i++)
	{
		cout << numbers[i] << ", ";
	}

	cout << endl;

	system("pause");
	return 0;
}

int fillArray(int numbers[20])
{
	for (int i = 0; i < 20; i++)
	{
		numbers[i] = rand() % 100 + 1;
	}

	return numbers[20];
}