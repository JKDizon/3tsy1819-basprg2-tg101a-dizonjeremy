#include <iostream>
#include <string>

using namespace std;

enum State
{
	Full,
	Healthy,
	Warning,
	Critical,
	Dead
};

State healthStatus(float statHP);

int main()
{
	float curHP, maxHP, statHP;

	cout << "Current HP: "; cin >> curHP;
	cout << "Max HP: "; cin >> maxHP;

	statHP = (curHP / maxHP) * 100;

	switch (healthStatus(statHP))
	{
	case Full: cout << "HP is 100% = Full" << endl; break;
	case Healthy: cout << "HP is 50% and above = Green (Healthy)" << endl; break;
	case Warning: cout << "HP is 20% to 49% = Yellow (Warning)" << endl; break;
	case Critical: cout << "HP is 1% to 19% = Red (Critical)" << endl; break;
	case Dead: cout << "HP is 0% = Dead" << endl; break;
	default: break;
	}

	system("pause");
	return 0;
}

State healthStatus(float statHP)
{
	State status;

	if (statHP == 100)
	{
		status = Full;
	}
	else if (statHP < 100 && statHP >= 50)
	{
		status = Healthy;
	}
	else if (statHP < 50 && statHP >= 20)
	{
		status =  Warning;
	}
	else if (statHP < 20 && statHP >= 1)
	{
		status = Critical;
	}
	else if (statHP == 0)
	{
		status = Dead;
	}

	return status;
}