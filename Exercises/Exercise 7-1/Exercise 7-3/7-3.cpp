#include <iostream>
#include <string>
#include <stdlib.h> 
#include <time.h>

using namespace std;

void exercise62();
void exercise63();

int main()
{
	int exercise;

	cout << "Select Exercise 6 (2-9): ";
	cin >> exercise;

	cin.clear();
	cin.ignore(1000, '\n');

	cout << endl;

	switch (exercise)
	{
	case 2: exercise62(); break;
	case 3: exercise63(); break;
	default: break;
	}

	cout << endl;

	system("pause");
	return 0;
}

void exercise62()
{
	int numbers[] = { 1, 2, 3, 4, 5 };
	int numbers2[] = { 0, 7, -10, 5, 3 };
	int sum[] = { 0 };

	for (int i = 0; i <= 4; i++) {
		sum[0] = sum[0] + numbers[i] + numbers2[i];
	}

	cout << "Sum: " << sum[0] << endl;
}

void exercise63()
{
	string items[] = { "Red Potion",
						"Blue Potion",
						"Yggdrasil Leaf",
						"Elixir",
						"Teleport Scroll",
						"Red Potion",
						"Red Potion",
						"Elixir"
						};
	string itemsSearch;
	bool searchResults = false;

	cout << "Search for: ";
	getline(cin, itemsSearch);

	for (int i = 0; i <= 7; i++) {
		if (itemsSearch == items[i]) {
			searchResults = true;
		}
	}

	if (searchResults) {
		cout << itemsSearch << " exists." << endl;
	}
	else {
		cout << itemsSearch << " does not exist." << endl;
	}

	return;
}