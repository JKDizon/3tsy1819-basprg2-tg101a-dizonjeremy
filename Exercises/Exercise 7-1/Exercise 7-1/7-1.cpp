#include <iostream>

using namespace std;

int computeFactorial(int number, int factorial);

int main()
{
	int number;
	int factorial = 1;

	cout << "Input a number: ";
	cin >> number;

	factorial = computeFactorial(number, factorial);

	cout << "Factorial of " << number << " = " << factorial <<endl;

	system("pause");
	return 0;
}

int computeFactorial(int number, int factorial)
{
	for (int x = number; x > 0; x = x - 1)
	{
		factorial = factorial * x;
	}

	return factorial;
}