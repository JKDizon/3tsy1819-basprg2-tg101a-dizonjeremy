#include <iostream>

using namespace std;

int main()
{
	int x, y;
	bool z;

	cout << "Input X:"; cin >> x;
	cout << endl << "Input Y:"; cin >> y;

	z = x == y;
	cout << endl << x << "==" << y << "=" << z;

	z = x != y;
	cout << endl << x << "!=" << y << "=" << z;

	z = x > y;
	cout << endl << x << ">" << y << "=" << z;

	z = x < y;
	cout << endl << x << "<" << y << "=" << z ;

	z = x<= y;
	cout << endl << x << "<=" << y << '=' << z ;

	system("pause");
	return 0; 
}
	
