#include <iostream>
#include <string>

using namespace std;

int main()
{
	char direction;
	int curLoc;
	int x = 0;
	int y = 0;

	cout << "Current Location:" << x << "," << y;
	cout << endl << "Where do you want to go? (N = north, E = east, W = west, S = south)";
	cin >> direction;

	switch (direction)
	{
	case 'N': case 'n':
		y++;
		cout << endl << "Current Location:" << x << "," << y;
		break;
	case 'E': case 'e':
		x++;
		cout << endl << "Current Location:" << x << "," << y;
		break;
	case 'W': case 'w':
		x--;
		cout << endl << "Current Location:" << x << "," << y;
		break;
	case 'S': case 's':
		y--;
		cout << endl << "Current Location:" << x << "," << y;
		break;
	default: cout << endl << "Invalid Direction!";
	}
	system("pause");
	return 0;
}