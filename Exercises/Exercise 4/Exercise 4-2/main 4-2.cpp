#include <iostream>

using namespace std;

int main()
{
	float maxHP, curHP, statHP;
	cout << "Current HP:"; cin >> curHP;

	cout << endl << "Max HP:"; cin >> maxHP;

	statHP = (curHP / maxHP) * 100;
	
	if (statHP == 100) { cout << endl << "HP is 100% = Full"; }

	else if (statHP < 100 && statHP >= 50){ cout << endl << "HP is 50% and above = Green (Healthy)"; }

	else if (statHP <= 49 && statHP >= 20) { cout << endl << "HP is 20% to 49% = Yellow (Warning)"; }

	else if (statHP <= 19 && statHP >= 1) { cout << endl << "HP is 1% to 19% = Red (Critical)"; }

	else if (statHP == 0) { cout << endl << "HP is 0% = Dead"; }

	system("pause");
	return 0; 
		
}