#include <iostream>
using namespace std;

int main()
{
	for (int rows = 1; rows <= 3; rows++) 
	{
		for (int spaces = 1; spaces <= 3 - rows; spaces++) 
		{
			cout << " ";
		}
		for (int star = 1; star <= 2 * rows - 1; star++) 
		{
			cout << "*";
		}
		cout << endl;
	}

	for (int rows = 2; rows >= 1; rows--) 
	{
		for (int spaces = 2; spaces >= rows; spaces--) 
		{
			cout << " ";
		}
		for (int star = 1; star <= 2 * rows - 1; star++) 
		{
			cout << "*";
		}
		cout << endl;
	}

	system("pause");
	return 0;
}