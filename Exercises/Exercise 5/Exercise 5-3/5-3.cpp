#include<iostream>
using namespace std;

int main()
{
	int value;
	int terms;
	int difference;
	int sum = 0;

	cout << "Input starting value: ";
	cin >> value;
	cout << "Input numbers of terms: ";
	cin >> terms;
	cout << "Input difference: ";
	cin >> difference;

	for (int x = terms; x > 0; x--)
	{
		sum = sum + value;
		value = value + difference;
	}
	
	cout << "Sum: " << sum << endl;

	system("pause");
	return 0;
}