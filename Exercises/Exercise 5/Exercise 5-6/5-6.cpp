#include <iostream>

using namespace std;

int main()
{
	for (int x = 5; x >= 1; x--) {
		for (int y = 1; y <= x; y++) {
			cout << "* ";
		}
		cout << endl;
	}

	system("pause");
	return 0;
}