#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
	int loopCounter;
	int value;
	double average = 0;
	int counter = 1;

	cout << "Input number of values to be computed: ";
	cin >> loopCounter;

	for (int x = loopCounter; x > 0; x = x - 1) {
		cout << "Input value " << counter << ": ";
		cin >> value;
		average = average + value;
		counter++;

	}

	average = average / loopCounter;

	cout << "Average: " << setprecision(3) << average << endl;

	system("pause");
	return 0;
}