#include <iostream>
using namespace std;

int main()
{
	for (int rows = 1; rows <= 4; rows++) 
	{
		for (int spaces = 1; spaces <= 4 - rows; spaces++) 
		{
			cout << " ";
		}
		for (int star = 1; star <= 2 * rows - 1; star++) 
		{
			cout << "*";
		}
	
		cout << endl;
	}

	system("pause");
	return 0;
}