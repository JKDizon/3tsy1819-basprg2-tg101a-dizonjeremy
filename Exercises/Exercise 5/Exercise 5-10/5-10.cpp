#include <iostream>
using namespace std;

int main()
{
	for (int star = 1; star <= 5; star++) 
	{
		cout << "*";
	}

	cout << endl;

	for (int rows = 1; rows <= 3; rows++) 
	{
		cout << "*";
		for (int spaces = 1; spaces <= 3; spaces++) 
		{
			cout << " ";
		}
		cout << "*";
		cout << endl;
	}

	for (int star = 1; star <= 5; star++) 
	{
		cout << "*";
	}
	
	cout << endl;

	system("pause");
	return 0;
}