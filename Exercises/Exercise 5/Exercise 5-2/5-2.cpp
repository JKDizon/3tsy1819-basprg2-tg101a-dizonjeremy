#include<iostream>
using namespace std;

int main()
{
	int number;
	int factorial = 1;

	cout << "Input a number: ";
	cin >> number;

	for (int x = number; x > 0; x = x - 1)
	{
		factorial = factorial * x;
	}
	cout << "Factorial of " << number << " = " << factorial << endl;

	system("pause");
	return 0;
}