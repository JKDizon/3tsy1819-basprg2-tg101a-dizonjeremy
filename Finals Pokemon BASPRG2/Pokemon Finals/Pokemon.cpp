#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <string>
#include <cstring>
#include <algorithm>

using namespace std;

enum pokemonTypes // Pokemon types
{
	NORMAL = 0,
	GRASS = 1,
	FIRE = 2,
	WATER = 3,
	BUG = 4,
	POISON = 5,
	ELECTRIC = 6,
};

enum mapPlaces // map locations
{
	PALLET = 1,
	ROUTE_1 = 2,
	VIRIDIAN = 3,
	ROUTE_2 = 4,
	VRD_FOREST = 5,
	PEWTER = 6,
	ROUTE_22 = 7,
};

struct Pokemon // struct for Pokemon
{
	string name;
	int type;
	int baseHp;
	int hp;
	int level;
	int baseDamage;
	int exp;
	int expToNextLevel;
};

int expChart(int level, int exp) // function to compute Experience to Next Level
{
	int nextLevel = level + 1;
	int requiredExp;

	int chart[100][2] = {
		{ 1, 0 },{ 2, 25 },{ 3, 50 },{ 4, 100 },{ 5, 150 },{ 6, 200 },{ 7, 400 },{ 8, 600 },{ 9, 800 },{ 10, 1000 },{ 11, 1500 },{ 12, 2000 },{ 13, 3000 },{ 14, 4000 },{ 15, 5000 },{ 16, 6000 },{ 17, 7000 },{ 18, 8000 },{ 19, 9000 },{ 20, 10000 },{ 21, 11500 },{ 22, 13000 },{ 23, 14500 },{ 24, 16000 },{ 25, 17500 },{ 26, 19000 },{ 27, 20500 },{ 28, 22000 },{ 29, 23500 },{ 30, 25000 },{ 31, 27500 },{ 32, 30000 },{ 33, 32500 },{ 34, 35000 },{ 35, 37500 },{ 36, 40000 },{ 37, 42500 },{ 38, 45000 },{ 39, 47500 },{ 40, 50000 },{ 41, 55000 },{ 42, 60000 },{ 43, 65000 },{ 44, 70000 },{ 45, 75000 },{ 46, 80000 },{ 47, 85000 },{ 48, 90000 },{ 49, 95000 },{ 50, 100000 },{ 51, 110000 },{ 52, 120000 },{ 53, 130000 },{ 54, 140000 },{ 55, 150000 },{ 56, 160000 },{ 57, 170000 },{ 58, 180000 },{ 59, 190000 },{ 60, 200000 },{ 61, 210000 },{ 62, 220000 },{ 63, 230000 },{ 64, 240000 },{ 65, 250000 },{ 66, 260000 },{ 67, 270000 },{ 68, 280000 },{ 69, 290000 },{ 70, 300000 },{ 71, 310000 },{ 72, 320000 },{ 73, 330000 },{ 74, 340000 },{ 75, 350000 },{ 76, 360000 },{ 77, 370000 },{ 78, 380000 },{ 79, 390000 },{ 80, 400000 },{ 81, 410000 },{ 82, 420000 },{ 83, 430000 },{ 84, 440000 },{ 85, 450000 },{ 86, 460000 },{ 87, 470000 },{ 88, 480000 },{ 89, 490000 },{ 90, 500000 },{ 91, 510000 },{ 92, 520000 },{ 93, 530000 },{ 94, 540000 },{ 95, 550000 },{ 96, 560000 },{ 97, 570000 },{ 98, 580000 },{ 99, 590000 },{ 100, 600000 }
	};

	for (int r = 0; r < 100; r++)
	{
		if (chart[r][0] == nextLevel)
		{
			requiredExp = chart[r][1];
		}
	}

	return requiredExp;
}

void setPokemon(vector<Pokemon> &allPokemon) // function to initialize Pokemon
{
	allPokemon[0].name = "Bulbasaur";
	allPokemon[0].type = GRASS;
	allPokemon[0].baseHp = 45;
	allPokemon[0].hp = 45;
	allPokemon[0].level = 5;
	allPokemon[0].baseDamage = 35;
	allPokemon[0].exp = 150;
	allPokemon[0].expToNextLevel = 50;

	allPokemon[1].name = "Ivysaur";
	allPokemon[1].type = GRASS;
	allPokemon[1].baseHp = 60;
	allPokemon[1].hp = 60;
	allPokemon[1].level = 16;
	allPokemon[1].baseDamage = 45;
	allPokemon[1].exp = 6000;
	allPokemon[1].expToNextLevel = 1000;

	allPokemon[1].name = "Venusaur";
	allPokemon[1].type = GRASS;
	allPokemon[1].baseHp = 80;
	allPokemon[1].hp = 80;
	allPokemon[1].level = 32;
	allPokemon[1].baseDamage = 80;
	allPokemon[1].exp = 30000;
	allPokemon[1].expToNextLevel = 2500;

	allPokemon[4].name = "Charmander";
	allPokemon[4].type = FIRE;
	allPokemon[4].baseHp = 39;
	allPokemon[4].hp = 39;
	allPokemon[4].level = 5;
	allPokemon[4].baseDamage = 40;
	allPokemon[4].exp = 150;
	allPokemon[4].expToNextLevel = 50;

	allPokemon[7].name = "Squirtle";
	allPokemon[7].type = WATER;
	allPokemon[7].baseHp = 44;
	allPokemon[7].hp = 44;
	allPokemon[7].level = 5;
	allPokemon[7].baseDamage = 35;
	allPokemon[7].exp = 150;
	allPokemon[7].expToNextLevel = 50;


	allPokemon[15].name = "Pidgey";
	allPokemon[15].type = NORMAL;
	allPokemon[15].baseHp = 40;
	allPokemon[15].hp = 40;
	allPokemon[15].level = 5;
	allPokemon[15].baseDamage = 35;
	allPokemon[15].exp = 150;
	allPokemon[15].expToNextLevel = 50;

	//expChart(allPokemon[7].level, allPokemon[7].exp);


	/*
	#001	#001	Bulbasaur	Bulbasaur	Grass	Poison
	#002	#002	Ivysaur	Ivysaur	Grass	Poison
	#003	#003	Venusaur	Venusaur	Grass	Poison
	#004	#004	Charmander	Charmander	Fire
	#005	#005	Charmeleon	Charmeleon	Fire
	#006	#006	Charizard	Charizard	Fire	Flying
	#007	#007	Squirtle	Squirtle	Water
	#008	#008	Wartortle	Wartortle	Water
	#009	#009	Blastoise	Blastoise	Water
	#010	#010	Caterpie	Caterpie	Bug
	#011	#011	Metapod	Metapod	Bug
	#012	#012	Butterfree	Butterfree	Bug	Flying
	#013	#013	Weedle	Weedle	Bug	Poison
	#014	#014	Kakuna	Kakuna	Bug	Poison
	#015	#015	Beedrill	Beedrill	Bug	Poison
	#016	#016	Pidgey	Pidgey	Normal	Flying
	#017	#017	Pidgeotto	Pidgeotto	Normal	Flying
	#018	#018	Pidgeot	Pidgeot	Normal	Flying
	#019	#019	Rattata	Rattata	Normal
	#020	#020	Raticate	Raticate	Normal
	#021	#021	Spearow	Spearow	Normal	Flying
	#022	#022	Fearow	Fearow	Normal	Flying
	#023	#023	Ekans	Ekans	Poison
	#024	#024	Arbok	Arbok	Poison
	#025	#025	Pikachu	Pikachu	Electric
	*/
}

void setStarterPokemon(vector<Pokemon> &allPokemon, vector<Pokemon> &trainerPokemon, int starterPokemon) // function to set trainer's starter Pokemon
{
	switch (starterPokemon)
	{
	case 1:
		trainerPokemon[0] = allPokemon[0];
		break;
	case 2:
		trainerPokemon[0] = allPokemon[4];
		break;
	case 3:
		trainerPokemon[0] = allPokemon[7];
		break;
	default: break;
	}
}

void setUserAction(int &action, int place) // function to ask user action
{
	cout << "What would you like to do?" << endl
		<< "[1] - Move              [2] - Pokemons" << endl;

	if (place == PALLET || place == VIRIDIAN || place == PEWTER)
	{
		cout << "[3] - Pokemon Center" << endl;
	}

	cin >> action;
}

void moveAction(int &locationx, int &locationy) // function to move trainer
{
	char direction;

	cout << "Where do you want to move?" << endl
		<< "[W] - Up \t [S] - Down \t [A] - Left \t [D] - Right" << endl;
	cin >> direction;

	while (direction != 'W' && direction != 'w' && direction != 'S' && direction != 's' && direction != 'A' && direction != 'a' && direction != 'D' && direction != 'd')
	{
		cout << "Invalid direction. Please try again."
			<< endl << endl;
		cout << "Where do you want to move?" << endl
			<< "[W] - Up \t [S] - Down \t [A] - Left \t [D] - Right" << endl;
		cin >> direction;
	}

	switch (direction)
	{
	case 'W': case 'w':
		locationy += 1;
		break;
	case 'S': case 's':
		locationy -= 1;
		break;
	case 'A': case 'a':
		locationx -= 1;
		break;
	case 'D': case 'd':
		locationx += 1;
		break;
	default:
		break;
	}
}

void showLocation(int locationx, int locationy, int &place) // function to show trainer's location
{
	int x = locationx + 14;
	int y = locationy + 3;
	int chance;

	int mapCoords[42][20] = {
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	};

	cout << "You are now at position (" << locationx << ", " << locationy << ")" << endl;

	switch (mapCoords[y][x])
	{
	case 0:
		cout << "You have reached the edge of the map." << endl;
		place = 0;
		break;
	case 1:
		cout << "You are in Pallet Town" << endl;
		place = PALLET;
		break;
	case 2:
		cout << "You are in Route 1" << endl;
		place = ROUTE_1;
		break;
	case 3:
		cout << "You are in Viridian City" << endl;
		place = VIRIDIAN;
		break;
	case 4:
		cout << "You are in Route 2" << endl;
		place = ROUTE_2;
		break;
	case 5:
		cout << "You are in Viridian Forest" << endl;
		place = VRD_FOREST;
		break;
	case 6:
		cout << "You are in Pewter City" << endl;
		place = PEWTER;
		break;
	case 7:
		cout << "You are in Route 22" << endl;
		place = ROUTE_22;
		break;
	default:
		break;
	}
}

void showStats(vector<Pokemon> &Pokemon, int size) // function to show Pokemon stats
{
	cout << endl << endl;

	for (int i = 0; i < size; i++)
	{
		cout << Pokemon[i].name << "'s stats:" << endl
			<< "Level: " << Pokemon[i].level << endl
			<< "Health Points: " << Pokemon[i].hp << "/" << Pokemon[i].baseHp << endl
			<< "Damage: ~" << Pokemon[i].baseDamage << endl
			<< "Experience: " << Pokemon[i].exp << "/" << expChart(Pokemon[i].level, Pokemon[i].exp) << endl;
	}

	cout << endl << endl;
}

void getWildPokemon(vector<Pokemon> &Pokemon, int place) // function to create wild Pokemon
{
	int wildPokemon;

	switch (place)
	{
	case ROUTE_1:
		wildPokemon = rand() % 100 + 1;

		if (wildPokemon < 50)
		{
			Pokemon[0].name = "Pidgey";
			Pokemon[0].type = NORMAL;
			Pokemon[0].baseHp = rand() % 40 + 1;
			Pokemon[0].hp = Pokemon[0].baseHp;
			Pokemon[0].level = rand() % 5 + 2;
			Pokemon[0].baseDamage = rand() % 45 + 1;
			Pokemon[0].exp = rand() % 150 + 1;
			Pokemon[0].expToNextLevel = expChart(Pokemon[0].level, Pokemon[0].exp) - Pokemon[0].exp;

		}

		else if (wildPokemon >= 50)
		{
			break;
		}

		break;
	default:
		break;
	}
}

int main()
{
	srand(time(0));

	string trainerName;
	int starterPokemon;
	int action;
	int locationx = 0;
	int locationy = 3;
	int place;
	int wildChance;

	vector<Pokemon> allPokemon(15); // vector to hold all Pokemon
	vector<Pokemon> trainerPokemon(1); // vector to hold trainer's Pokemon
	vector<Pokemon> wildPokemon(1); // vector to hold wild Pokemon

	setPokemon(allPokemon); // initialize Pokemon

	cout << "Hi trainer! What is your name?" << endl;
	cin >> trainerName;

	cin.clear(); // system("pause");
	cin.ignore(); // system("cls");

	cout << "I have three (3) Pokemons here." << endl
		<< "You can have one! Go on, choose!"
		<< endl << endl << endl
		<< "1 - Bulbasaur \t\t Grass Type \t Level 5" << endl
		<< "2 - Charmander \t\t Fire Type \t Level 5" << endl
		<< "3 - Squirtle \t\t Water Type \t Level 5"
		<< endl << endl << endl
		<< "Input choice: ";
	cin >> starterPokemon;

	while (starterPokemon != 1 && starterPokemon != 2 && starterPokemon != 3)
	{
		cout << "Invalid choice. Please try again."
			<< endl << endl
			<< "Input choice: ";
		cin >> starterPokemon;
	}

	setStarterPokemon(allPokemon, trainerPokemon, starterPokemon); // set trainer's starter Pokemon

	cout << "You chose " << trainerPokemon[0].name << "!" << endl;

	cin.clear(); // system("pause");
	cin.ignore(); // system("cls");

	cout << "Your journey begins now!" << endl;

	cin.clear(); // system("pause");
	cin.ignore(); // system("cls");

	do
	{
		setUserAction(action, place); // get user actions

		while (action != 1 && action != 2 && action != 3)
		{
			cout << action << "Invalid choice. Please try again."
				<< endl << endl;
			setUserAction(action, place);
		}

		cin.clear(); // system("pause");
		cin.ignore(); // system("cls");

		switch (action)
		{
		case 1:
			moveAction(locationx, locationy); // move trainer
			showLocation(locationx, locationy, place); // show trainer's location

			wildChance = rand() % 100 + 1; // roll for wild Pokemon encounter

			cout << wildChance << endl;

			if ((place == ROUTE_1 || place == ROUTE_2 || place == VRD_FOREST || place == ROUTE_22) && wildChance >= 50)
			{
				getWildPokemon(wildPokemon, place);
			}

			break;
		case 2:
			showStats(trainerPokemon, trainerPokemon.size()); // show trainer's Pokemon
			break;
		case 3:
			break;
		default:
			break;
		}

		cin.clear(); // system("pause");
		cin.ignore(); // system("cls");

	} while (action != 4);

	return 0;
}